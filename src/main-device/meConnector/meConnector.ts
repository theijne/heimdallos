import * as component from "component"

let me: ME.Controller

export function Initialize() {
    if (component.isAvailable("me_controller") == false) {
        print("[FAIL] ME System is unavailable. Exiting");
        os.exit();
    }
    
    me = component.getPrimary("me_controller");
    print(`[OK] Connected ME system`);
    
    // for (let c of me.getItemsInNetwork({ name: "minecraft:stone" })) {
    //     print(`${c.label}: ${c.size}`);
    // }
    
    // for (let c of me.getCpus()) {
    //     print(`${c.name}: ${c.busy}`);
    // }
}


