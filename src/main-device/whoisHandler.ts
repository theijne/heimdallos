
import { modem } from "component";
import { serialize } from "serialization";
import { ModemEventPayload, MessageBody } from "../common/modem";
import { MessageActions } from "../common/constants";

export function WhoIsMasterHandler(payload: ModemEventPayload) {
    let response: MessageBody = {
       action: MessageActions.AnnounceMaster.toString()
    }

    modem.send(payload.remoteAddress, payload.port, serialize(response))
}