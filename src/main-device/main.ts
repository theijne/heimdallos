import * as event from "event";
import * as shell from "shell";

import * as modem from "./../common/modem";
import * as me from "./meConnector/meConnector";

import * as config from "./../common/config"
import { isControlDown } from "keyboard";
import { computer } from "component";
import { WhoIsMasterHandler } from "./whoisHandler";

// Clear screen
shell.execute("clear")

let configMap = {
  port: -1
}

if(!config.ConfigExists()) {
  print("Config does not exist. Please enter the following details:")

  print("Network port: ")
  let input = io.read();
  if(input == null || input == undefined || input == "") {
    print("Didn't input shit. Fuck you!")
  }
  
  configMap.port = parseInt(tostring(input))
  config.SaveConfig(configMap)
} else {
  configMap = config.LoadConfig()
}

modem.Initialize(configMap.port);
me.Initialize()

modem.RegisterListener("whois_master", WhoIsMasterHandler)
modem.RegisterListener("test", (payload: modem.ModemEventPayload) => {
  computer.beep()
})

while (true) { 
  let [_, __, ___, key, ____] = event.pull("key_down")

  if(key == 20 && isControlDown()) {
    os.exit()
  }
}