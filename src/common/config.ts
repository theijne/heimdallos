import { filesystem } from "component";
import { serialize, unserialize } from "serialization";

const configFile = "./system.config"

export function ConfigExists(): boolean {
    return filesystem.exists(configFile)
}

export function LoadConfig(): any {
    if (!ConfigExists()) {
        return null;
    }

    let file = filesystem.open(configFile, "r")
    let data = filesystem.read(file, 1000)

    if (data == null) {
        print("[FAIL] Unable to load config")
        return null
    }

    filesystem.close(file)

    data = unserialize(data)
    print("[OK] Config loaded")
    return data
}

export function SaveConfig(config: any): boolean {
    if (ConfigExists()) {
        filesystem.remove(configFile)
    }

    let file = filesystem.open(configFile, "w")
    if (!filesystem.write(file, serialize(config))){
        print("[FAIL] Didn't write config to disk")
    } else {
        print("[OK] Config stored")
    }

    filesystem.close(file)

    return false;
}