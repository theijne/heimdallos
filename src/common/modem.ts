import * as component from "component";
import * as thread from "thread"
import * as event from "event"
import { serialize, unserialize } from "serialization";
import { MessageActions } from "./constants";

let modem: OpenOS.Modem;
let port: number;

let messageEvents: MessageEvent[] = []

interface MessageEvent {
  name: string
  callbacks: Function[]
}

export interface MessageBody { 
  action: string;
  payload?: any;
}

export interface ModemEventPayload {
  remoteAddress: string;
  port: number;
  payload: any;
}

export function Initialize(p: number) {
  port = p;
  modem = component.getPrimary("modem");
  modem.open(port);

  if (modem.isOpen(port) == false) {
    if (modem.open(port) == false) {
      print("[FAIL] Unable to open network on port: " + port);
      os.exit();
    }
  }

  modem.broadcast(port, serialize({"eventName": MessageActions.AnnounceMaster}));
  print("[OK] Opening network on port: " + port);

  const t = thread.create(listenForMessages)
}

export function RegisterListener(messageName: string, callback: Function) {
  let eventIndex = messageEvents.findIndex((ev) => ev.name == messageName)
  if(eventIndex == -1) {
    // Doesn't exist. Let's create
    let newEvent: MessageEvent = {
      name: messageName, 
      callbacks: []
    }

    newEvent.callbacks.push(callback)
    messageEvents.push(newEvent)
    return
  }

  messageEvents[eventIndex].callbacks.push(callback)
}

export function RouteMessage(
  remoteAddress: string,
  port: number,
  message: any
) {
  if(message == undefined) {
    print(`[WARN] Message received with no body from ${remoteAddress}`)
    return
  }

  print(`[OK] Received from ${remoteAddress}:${port} with payload ${message}`)
  let data: MessageBody = unserialize(message);

  if(data.action == undefined || data.action == "") {
    print(`[WARN] Received invalid message name ${data.action} from ${remoteAddress}`)
    return;
  }

  let mPayload: ModemEventPayload = {
    remoteAddress: remoteAddress,
    port: port,
    payload: message
  }

  emitEvent(data.action, mPayload)
}

function emitEvent(eventName: string, payload: ModemEventPayload) {
  let eventHolder = messageEvents.find((ev) => ev.name == eventName)
  if(eventHolder == undefined) {
    print(`[WARN] Unable to route event "${eventName}". No event listed`)
    return
  }

  if(eventHolder.callbacks.length == 0) {
    print(`[WARN] Unable to route event "${eventName}". No listeners defined`)
    return
  }

  print(`[OK] Routing ${eventName} to ${eventHolder.callbacks.length} listeners`)
  eventHolder.callbacks.forEach((cb: any) => cb(payload))
} 

function listenForMessages() {
  print("[OK] Listening for incoming messages")

  while(true) {
    let [_, __, from, port, ___, message] = event.pull("modem_message")
    RouteMessage(from, port, message)
  }
}
